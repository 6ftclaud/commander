import logging

import discord
from discord.ext import commands
from os import environ


async def send_embed(ctx, embed):
    """
    Function that handles the sending of embeds
    -> Takes context and embed to send
    - tries to send embed in channel
    - tries to send normal message when that fails
    - tries to send embed private with information abot missing permissions
    """
    try:
        await ctx.send(embed=embed)
    except Forbidden:
        try:
            await ctx.send("It seems like I can't send embeds. Please check my permissions.")
        except Forbidden:
            await ctx.author.send(
                f"Seems like I can't send any message in {ctx.channel.name} on {ctx.guild.name}\n", embed=embed)

class misc(commands.Cog):
    """
    Various miscellaneous commands
    """

    def __init__(self, client):
        self.client = client

    """ Events """
    # Send message on cog ready
    @commands.Cog.listener()
    async def on_ready(self):
        logging.info(f"misc.py: misc cog is online")

    # Check latency
    @commands.command()
    async def ping(self, ctx):
        """
        Checks bot latency
        """
        logging.info(f"misc.py: ?ping called by {ctx.message.author}")
        await ctx.send(f"I'm alive!\nResponse time is {round(self.client.latency, 2)}s")

    @commands.command()
    async def convert(self, ctx, *args):
        """
        Will convert bytes to KB, MB and GB
        """
        # ctx - Discord context
        command = ' '.join(args).split()
        command[0] = int(command[0])
        kilobytes = command[0] / 1024
        megabytes = command[0] / 1024 / 1024
        gigabytes = command[0] / 1024 / 1024 / 1024
        message = f"Bytes conversion:```js\nBytes: {command[0]}\nKiloBytes: {kilobytes}\nMegaBytes: {megabytes}\nGigaBytes: {gigabytes}```"
        await ctx.send(message)

    @commands.command()
    async def help(self, ctx, *input):

        """Shows information of all modules"""
	
        prefix = '?'
        version = environ.get('VERSION', 'unknown')

        owner = None
        owner_name = "6ftClaud"

        if not input:

            # starting to build embed
            emb = discord.Embed(title='Commands and modules', color=discord.Color.blue(),
                                description=f'Use `{prefix}help <module>` to gain more information about that module')

            # iterating trough cogs, gathering descriptions
            cogs_desc = ''
            unmonitored_cogs = ["Monitor"]
            for cog in self.client.cogs:
                if cog not in unmonitored_cogs:
                    # cogs_desc += f'`{cog}` {self.client.cogs[cog].__doc__}\n' # With description
                    cogs_desc += f'`{cog}`\n'

            # adding 'list' of cogs to embed
            emb.add_field(name='Modules', value=cogs_desc, inline=False)

            # integrating trough uncategorized commands
            commands_desc = ''
            for command in self.client.walk_commands():
                # if cog not in a cog
                # listing command if cog name is None and command isn't hidden
                if not command.cog_name and not command.hidden:
                    commands_desc += f'{command.name} - {command.help}\n'

            # adding those commands to embed
            if commands_desc:
                emb.add_field(name='Not belonging to a module', value=commands_desc, inline=False)

            # setting information about author
            emb.add_field(name="About", value=f"The Bot is developed by Claud#9034.\n\
            Please visit https://gitlab.com/6ftclaud/commander to submit ideas or bugs.")
            emb.set_footer(text=f"Bot is running version {version}")

        # block called when one cog-name is given
        # trying to find matching cog and it's commands
        elif len(input) == 1:

            # iterating trough cogs
            for cog in self.client.cogs:
                # check if cog is the matching one
                if cog.lower() == input[0].lower():

                    # making title - getting description from doc-string below class
                    emb = discord.Embed(title=f'{cog} - Commands', description=self.client.cogs[cog].__doc__,
                                        color=discord.Color.green())

                    # getting commands from cog
                    for command in self.client.get_cog(cog).get_commands():
                        # if cog is not hidden
                        if not command.hidden:
                            emb.add_field(name=f"`{prefix}{command.name}`", value=command.help, inline=False)
                    # found cog - breaking loop
                    break

            # if input not found
            else:
                emb = discord.Embed(title="What's that?!",
                                    description=f"I've never heard from a module called `{input[0]}` before!",
                                    color=discord.Color.orange())

        # too many cogs requested - only one at a time allowed
        elif len(input) > 1:
            emb = discord.Embed(title="That's too much.",
                                description="Please request only one module at once :sweat_smile:",
                                color=discord.Color.orange())

        else:
            emb = discord.Embed(title="It's a magical place.",
                                description="I don't know how you got here. But I didn't see this coming at all.\n"
                                            "Would you please be so kind to report that issue to me on gitlab?\n"
                                            "https://gitlab.com/6ftclaud/commander\n"
                                            "Thank you!",
                                color=discord.Color.red())

        # sending reply embed using our own function defined above
        await send_embed(ctx, emb)




def setup(client):
    client.add_cog(misc(client))
    