# Discord developer portal
# https://discord.com/developers/applications

# Invite link
# https://discord.com/api/oauth2/authorize?client_id=941653484414050364&permissions=8&scope=bot

import logging
import sys
# Modules for handling env variables
from os import environ

# Discord modules
import discord
from discord.ext import commands

# Set up logging
environment = environ.get('environment')  # Get current environment from .env
# Set loglevel according to environment
logLevel = logging.INFO if environment == 'prod' else logging.DEBUG
logging.basicConfig(
    level=logLevel,
    format='%(asctime)s: (%(levelname)s) %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p',
    handlers=[
        logging.FileHandler("execution.log", "a"),
        logging.StreamHandler(sys.stdout)
    ])

# Set default intents
intents = discord.Intents().default()
# Create client object
client = commands.Bot(command_prefix='?', intents=intents)
client.remove_command('help')
# Load Cogs (modules)
client.load_extension('misc')
client.load_extension('configure')
client.load_extension('monitor')
client.load_extension('Portainer')
client.load_extension('Proxmox')


# Send message to console on ready
@client.event
async def on_ready():
    logging.info('main.py: Logged in as {0.user}'.format(client))


# Load token from environment variables
token = environ.get("DISCORD_TOKEN")
# Run the client
client.run(token)

