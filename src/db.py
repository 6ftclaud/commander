import logging
import sys
from os import environ
from time import sleep

import mysql.connector

logLevel = logging.DEBUG
logging.basicConfig(
    level=logLevel,
    format='%(asctime)s: (%(levelname)s) %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p',
    handlers=[
        logging.FileHandler("execution.log", "a"),
        logging.StreamHandler(sys.stdout)
    ])


class Database():

    def __init__(self):
        self.database = self.Connect()
        self.cursor = self.database.cursor()
        # Database structure
        self.tables = {
            "User": [("discord_id", "BIGINT")],
            "Service": [("user", "BIGINT"), ("name", "text"), ("url", "text"), ("token", "text")],
            "VM_State": [("user", "BIGINT"), ("node", "text"), ("name", "text"), ("state", "bool")],
            "Container_State": [("user", "BIGINT"), ("env", "VARCHAR(3)"), ("name", "text"), ("state", "bool")],
        }
        self.cursor.execute("SHOW TABLES")
        results = self.cursor.fetchall()
        # If there're tables, do nothing
        if results:
            logging.info("Tables already exist")
        # If there are no tables, create them
        else:
            self.Initialise()

    def Connect(self):
        """
        Will establish a connection to the database service
        """
        try:
            logging.info(f"Connecting to database with HOST: {environ.get('MYSQL_HOST')}")
            logging.info(f"Connecting to database with USER: {environ.get('MYSQL_USER')}")
            logging.debug(f"Connecting to database with PASSWORD: {environ.get('MYSQL_PASSWORD')}")
            logging.info(f"Connecting to database with DATABASE: {environ.get('MYSQL_DATABASE')}")
            database = mysql.connector.connect(
                host=environ.get('MYSQL_HOST', 'localhost'),
                user=environ.get('MYSQL_USER', 'commander'),
                password=environ.get('MYSQL_PASSWORD', 'dev'),
                database=environ.get('MYSQL_DATABASE', 'commander'),
                autocommit=True
            )
            logging.info("Succesfully connected to the database")
        except:
            logging.error("Error connecting to database")
            exit(1)
        return database

    def Initialise(self):
        """
        Will create the tables according to the self.tables structure
        """
        logging.info("Initializing database tables")
        for index, table in enumerate(self.tables.values()):
            table_name = list(self.tables.keys())[index]
            command = f"CREATE TABLE {table_name} ("
            if table_name == "User":
                command = command + "discord_id BIGINT PRIMARY KEY)"
            else:
                for var in table:
                    command = command + f"{var[0]}" + f" {var[1]} NOT NULL, "
                command = command + \
                    "FOREIGN KEY (user) REFERENCES User (discord_id) ON DELETE CASCADE)"
            logging.debug(command)
            self.cursor.execute(command)

    def DBCommand(self, command, retrieveAll=None):
        """
        Will execute and log the given command
        command - the SQL command
        retrieveAll - will return all the information
        """
        try:
            self.cursor.execute(command)
            if retrieveAll:
                return [x for x in self.cursor]
            else:
                return self.cursor.fetchone()
        except:
            return False
        finally:
            logging.debug(f"SQL command executed: {command};")