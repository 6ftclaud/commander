import json
import logging
from os import getenv

import discord
import requests
from cryptocode import decrypt
from discord.ext import commands

import db


class Proxmox(commands.Cog):
    """
    The main module that controls Proxmox VMs and LXCs
    """

    def __init__(self, client):
        self.db = db.Database()
        self.colors = {
            "proxmox": 0xF06F00,
            "success": 0x00ff00,
            "error": 0xff0000
        }

    """ Events """
    # Send message on cog ready
    @commands.Cog.listener()
    async def on_ready(self):
        logging.info("Proxmox.py: Proxmox cog is online")

    @commands.command()
    async def proxmox(self, ctx, *args):
        """
        * For VM commands use vm, for LXC commands use lxc
        List VM/LXC: `?proxmox list vm`
        Start VM/LXC: `?proxmox start vm <name>`
        Retart VM/LXC: `?proxmox reboot vm <name>`
        Shutdown VM/LXC: `?proxmox shutdown vm <name>`
        Stop VM/LXC: `?proxmox stop vm <name>`
        Reset VM/LXC: `?proxmox reset vm <name>`
        Get stats: `?proxmox stats vm <name>`
        """
        logging.info(
            f"Proxmox.py: ?proxmox called by {ctx.message.author}")
        command = ' '.join(args).lower().split()

        if len(command) < 1:
            return

        # Get user's url and token information
        url = self.db.DBCommand(
            f"SELECT url FROM Service WHERE user={ctx.author.id} AND name='Proxmox'", retrieveAll=True)[0][0]
        token = self.db.DBCommand(
            f"SELECT token FROM Service WHERE user={ctx.author.id} AND name='Proxmox'", retrieveAll=True)[0][0]
        token = decrypt(token, getenv('ENCRYPT'))

        # List all the VMs or LXCs
        if command[0] == "list":
            if command[1] == "vm":
                VMs = self.GetVM(url, token)
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["proxmox"], title="Virtual Machines", description="A list of your virtual machines")
                for VM in VMs:
                    value = f"\
                        VM ID: {VM['vmid']}\n\
                        Status: {VM['status']}\n\
                        CPUs: {VM['cpus']}\n\
                        Memory: {VM['maxmem']} MB"
                    responseMessage.add_field(
                        name=VM['name'], value=value)
                await ctx.send(embed=responseMessage)
            elif command[1] == "lxc":
                LXCs = self.GetLXC(url, token)
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["proxmox"], title="LXC Containers", description="A list of your LXC Containers")
                for LXC in LXCs:
                    value = f"\
                        LXC ID: {LXC['vmid']}\n\
                        Status: {LXC['status']}\n\
                        CPUs: {LXC['cpus']}\n\
                        Memory: {LXC['maxmem']} MB"
                    responseMessage.add_field(
                        name=LXC['name'], value=value)
                await ctx.send(embed=responseMessage)

        # Start a VM or LXC
        elif command[0] == "start":
            if command[1] == "vm":
                VMs = self.GetVM(url, token)
                code = self.ControlVM(
                    url, token, VMs, "start", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "VM successfully started")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)
            elif command[1] == "lxc":
                LXC = self.GetLXC(url, token)
                code = self.ControlLXC(
                    url, token, LXCs, "start", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "LXC container successfully started")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)
        
        # Restart a VM or LXC
        elif command[0] == "reboot":
            if command[1] == "vm":
                VMs = self.GetVM(url, token)
                code = self.ControlVM(
                    url, token, VMs, "reboot", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "VM successfully rebooted")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)
            elif command[1] == "lxc":
                LXC = self.GetLXC(url, token)
                code = self.ControlLXC(
                    url, token, LXCs, "reboot", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "LXC container successfully rebooted")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)

        # Shutdown a VM or LXC
        elif command[0] == "shutdown":
            if command[1] == "vm":
                VMs = self.GetVM(url, token)
                code = self.ControlVM(
                    url, token, VMs, "shutdown", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "VM shutdown successful")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)
            elif command[1] == "lxc":
                LXC = self.GetLXC(url, token)
                code = self.ControlLXC(
                    url, token, LXCs, "shutdown", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "LXC container shutdown successful")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)

        # Stop a VM or LXC
        elif command[0] == "stop":
            if command[1] == "vm":
                VMs = self.GetVM(url, token)
                code = self.ControlVM(
                    url, token, VMs, "stop", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "VM stopped successfully")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)
            elif command[1] == "lxc":
                LXC = self.GetLXC(url, token)
                code = self.ControlLXC(
                    url, token, LXCs, "stop", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "LXC container stopped successfully")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)

        # Reset a VM or LXC
        elif command[0] == "reset":
            if command[1] == "vm":
                VMs = self.GetVM(url, token)
                code = self.ControlVM(
                    url, token, VMs, "reset", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "VM reset successful")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)
            elif command[1] == "lxc":
                LXC = self.GetLXC(url, token)
                code = self.ControlLXC(
                    url, token, LXCs, "reset", command[2], ctx.author.id)
                if code == 200:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["success"], "Success", "LXC container reset successful")
                    await ctx.send(embed=responseMessage)
                else:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], "Error", "Bad request")
                    await ctx.send(embed=responseMessage)

        # Get VM or LXC stats
        elif command[0] == "stats":
            if command[1] == "vm":
                stats = self.GetStats(url, token, "VM", command[2])
                responseMessage = self.EmbedMessage(
                    ctx, self.colors["proxmox"], f"{command[2]} stats:", f"```json\n{stats}```")
                try:
                    await ctx.send(embed=responseMessage)
                except discord.errors.HTTPException:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], f"Error", "Response body is too long to send via Discord")
                    await ctx.send(embed=responseMessage)
            elif command[1] == "lxc":
                stats = self.GetStats(url, token, "LXC", command[2])
                responseMessage = self.EmbedMessage(
                    ctx, self.colors["proxmox"], f"{command[2]} stats:", f"```json\n{stats}```")
                try:
                    await ctx.send(embed=responseMessage)
                except discord.errors.HTTPException:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], f"Error", "Response body is too long to send via Discord")
                    await ctx.send(embed=responseMessage)

    def GetNodes(self, url, token):
        """
        Gets Proxmox nodes (servers)
        url - service url
        token - service API key
        """
        url = url + "api2/json/nodes"
        headers = {
            'Authorization': f'PVEAPIToken={token}'
        }
        response = requests.request("GET", url, headers=headers).json()
        nodes = []
        for entry in response["data"]:
            nodes.append(entry["node"])
        return nodes

    def GetVM(self, url, token):
        """
        Gets VMs from nodes
        url - service url
        token - service API token
        """
        nodes = self.GetNodes(url, token)
        headers = {
            'Authorization': f'PVEAPIToken={token}'
        }
        VMs = []
        for node in nodes:
            url = url + f"api2/json/nodes/{node}/qemu"
            response = requests.request("GET", url, headers=headers).json()
            for entry in response["data"]:
                VM = {
                    "name": entry["name"],  # Name
                    "node": node,  # Server node
                    "vmid": entry["vmid"],  # ID
                    "status": entry["status"],  # Current status
                    "cpus": entry["cpus"],  # Allocated CPUs
                    # Allocated RAM in megabytes
                    "maxmem": str(int(entry["maxmem"])/1048576),
                    "uptime": entry["uptime"]  # Uptime in seconds
                }
                # Append VMs to array
                VMs.append(VM)
        # Return sorted list
        return sorted(VMs, key=lambda d: d["vmid"])

    def ControlVM(self, url, token, VMs, action, name, author_id):
        """
        Start, Restart, Shutdown, Stop or Restart a VM
        url - service url
        token - service API token
        VMs - list of VMs
        action - start, reboot, shutdown, stop or reset
        name - VM name
        author_id - used to find appropriate values to update in the database
        """
        headers = {
            'Authorization': f'PVEAPIToken={token}'
        }
        for VM in VMs:
            if VM["name"] == name:
                url = url + \
                    f"api2/json/nodes/{VM['node']}/qemu/{VM['vmid']}/status/{action}"
                response = requests.request(
                    "POST", url, headers=headers).status_code
                state = 1 if action in ["start", "reboot", "reset"] else 0
                if response == 200:
                    command = f"UPDATE VM_State SET state={state} WHERE user={author_id} AND node='{VM['node']}' AND name='{VM['name']}'"
                    self.db.DBCommand(command)
                return response
        return 404

    def GetLXC(self, url, token):
        """
        Gets LXCs from nodes
        url - service url
        token - service API token
        """
        nodes = self.GetNodes(url, token)
        headers = {
            'Authorization': f'PVEAPIToken={token}'
        }
        LXCs = []
        for node in nodes:
            url = url + f"api2/json/nodes/{node}/lxc"
            response = requests.request("GET", url, headers=headers).json()
            for entry in response["data"]:
                LXC = {
                    "name": entry["name"],  # Name
                    "node": node,  # Server node
                    "vmid": entry["vmid"],  # ID
                    "status": entry["status"],  # Current status
                    "cpus": entry["cpus"],  # Allocated CPUs
                    # Allocated RAM in megabytes
                    "maxmem": str(int(entry["maxmem"])/1048576),
                    "uptime": entry["uptime"]  # Uptime in seconds
                }
                # Append LXCs to array
                LXCs.append(LXC)
        # Return sorted list
        return sorted(LXCs, key=lambda d: d["vmid"])

    def ControlLXC(self, url, token, LXCs, action, name, author_id):
        """
        Start, Restart, Shutdown, Stop or Restart an LXC
        url - service url
        token - service API token
        LXCs - list of LXCs
        action - start, reboot, shutdown, stop or reboot
        name - LXC name
        author_id - used to find appropriate values to update in the database
        """
        headers = {
            'Authorization': f'PVEAPIToken={token}'
        }
        for LXC in LXCs:
            if LXC["name"] == name:
                url = url + \
                    f"api2/json/nodes/{LXC['node']}/qemu/{LXC['vmid']}/status/{action}"
                response = requests.request(
                    "POST", url, headers=headers).status_code
                state = 1 if action in ["start", "reboot", "reset"] else 0
                if response == 200:
                    command = f"UPDATE VM_State SET state={state} WHERE user={author_id} AND node='{VM['node']}' AND name='{VM['name']}'"
                    self.db.DBCommand(command)
                return response
        return 404

    def GetStats(self, url, token, VM_type, name):
        """
        Get VM or LXC stats
        url - service url
        token - service API token
        VM_type - VM or LXC
        name - VM/LXC name
        """
        headers = {
            'Authorization': f'PVEAPIToken={token}'
        }
        if VM_type == "VM":
            VMs = self.GetVM(url, token)
            for VM in VMs:
                if VM["name"].lower() == name:
                    url = url + \
                        f"api2/json/nodes/{VM['node']}/qemu/{VM['vmid']}/status/current"
                    stats = requests.request(
                        "GET", url, headers=headers).json()
                    return json.dumps(stats["data"], sort_keys=True, indent=4)
        elif VM_type == "LXC":
            LXCs = self.GetLXC(url, token)
            for LXC in LXCs:
                if LXC["name"].lower() == name:
                    url = url + \
                        f"api2/json/nodes/{LXC['node']}/qemu/{LXC['vmid']}/status/current"
                    stats = requests.request(
                        "GET", url, headers=headers).json()
                    return json.dumps(stats["data"], sort_keys=True, indent=4)

    def EmbedMessage(self, ctx, color, title, description):
        responseMessage = discord.Embed(color=color, title=title, description=description).set_author(
            name=ctx.message.author, icon_url=ctx.author.avatar_url)
        return responseMessage


def setup(client):
    client.add_cog(Proxmox(client))
    