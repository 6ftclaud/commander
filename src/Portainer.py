import json
import logging
from os import getenv

import discord
import requests
from cryptocode import decrypt
from discord.ext import commands

import db


class Portainer(commands.Cog):
    """
    Main portainer commands module
    """

    def __init__(self, client):
        self.client = client
        self.db = db.Database()
        self.colors = {
            "portainer": 0x25b8eb,
            "success": 0x00ff00,
            "error": 0xff0000
        }

    """ Events """
    # Send message on cog ready
    @commands.Cog.listener()
    async def on_ready(self):
        logging.info("Portainer.py: Portainer cog is online")

    @commands.command()
    async def portainer(self, ctx, *args):
        """
        List containers: `?portainer list`
        Start/Restart container: `?portainer start <name>`
        Stop container: `?portainer stop <name>`
        Get logs: `?portainer logs <name>`
        Get stats: `?portainer stats <name>`
        """
        logging.info(
            f"Portainer.py: ?portainer called by {ctx.message.author}")
        command = ' '.join(args).split()

        if len(command) < 1:
            return
        
        # Get user's url and token information
        url = self.db.DBCommand(
            f"SELECT url FROM Service WHERE user={ctx.author.id} AND name='Portainer'", retrieveAll=True)[0][0]
        token = self.db.DBCommand(
            f"SELECT token FROM Service WHERE user={ctx.author.id} AND name='Portainer'", retrieveAll=True)[0][0]
        token = decrypt(token, getenv('ENCRYPT'))

        endpoints = self.GetEndpoints(url, token)

        # List the running containers
        if command[0] == "list":
            if len(command) >= 3:
                containers = self.GetContainers(
                    url, token, endpoints, command[1], command[2])
            else:
                containers = self.GetContainers(url, token, endpoints)
            responseMessage = self.EmbedMessage(
                ctx, color=self.colors["portainer"], title="Containers", description="A list of your containers")
            for container in containers:
                field = "**Image: **" + container[1] + "\n" + "**State: **" + \
                    container[2] + "\n" + "**Status: **" + container[3] + "\n"
                responseMessage.add_field(
                    name=container[0], value=''.join(field))
            await ctx.send(embed=responseMessage)

        # Start a container
        elif command[0] == "start" or command[0] == "restart":
            if len(command) < 2:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description="You must provide a name for the container\n**Usage**: `?portainer start plex`")
                await ctx.send(embed=responseMessage)
                return
            responseCode = self.StartContainer(
                url, token, endpoints, command[1], ctx.author.id)
            if responseCode == 204:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["success"], title="Success", description=f"Container **{command[1]}** successfully started")
                await ctx.send(embed=responseMessage)
            if responseCode == 404:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description=f"Container **{command[1]}** does not exist")
                await ctx.send(embed=responseMessage)

        # Stop a container
        elif command[0] == "stop":
            if len(command) < 2:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description="You must provide a name for the container\n**Usage**: `?portainer stop plex`")
                await ctx.send(embed=responseMessage)
                return
            responseCode = self.StopContainer(
                url, token, endpoints, command[1], ctx.author.id)
            if responseCode == 204:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["success"], title="Success", description=f"Container **{command[1]}** successfully stopped")
                await ctx.send(embed=responseMessage)
            if responseCode == 304:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description=f"Container **{command[1]}** is already stopped")
                await ctx.send(embed=responseMessage)
            if responseCode == 404:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description=f"Container **{command[1]}** does not exist")
                await ctx.send(embed=responseMessage)
            if responseCode == 400:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description=f"Bad request")
                await ctx.send(embed=responseMessage)

        # Get container logs
        elif command[0] == "logs":
            if len(command) < 2:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description="You must provide a name for the container\n**Usage**: `?portainer logs plex`")
                await ctx.send(embed=responseMessage)
                return
            response = self.ContainerLogs(url, token, endpoints, command[1])
            if response == 404:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description=f"Container **{command[1]}** does not exist")
                await ctx.send(embed=responseMessage)
            else:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["portainer"], title="Success", description=f"Container **{command[1]}** logs retrieved:\n```\n{response}```")
                await ctx.send(embed=responseMessage)

        # Get stats of container
        elif command[0] == "stats":
            if len(command) < 2:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description="You must provide a name for the container\n**Usage**: `?portainer stats plex`")
                await ctx.send(embed=responseMessage)
                return
            stats = self.GetStats(url, token, endpoints, command[1])
            if stats:
                responseMessage = self.EmbedMessage(
                    ctx, self.colors["portainer"], f"{command[1]} stats:", f"```json\n{stats}```")
                try:
                    await ctx.send(embed=responseMessage)
                except discord.errors.HTTPException:
                    responseMessage = self.EmbedMessage(
                        ctx, self.colors["error"], f"Error", "Response body is too long to send via Discord")
                    await ctx.send(embed=responseMessage)
            else:
                responseMessage = self.EmbedMessage(
                    ctx, color=self.colors["error"], title="Error", description=f"Bad Request")
                await ctx.send(embed=responseMessage)

    def GetEndpoints(self, url, token):
        """
        Gets endpoints from Portainer API
        url - service url
        token - service API token
        """
        url = url + "api/endpoints"
        headers = {'X-API-Key': f'{token}'}
        endpoints = []
        response = requests.get(url, headers=headers).json()
        for endpoint in response:
            endpoints.append(endpoint["Id"])
        return endpoints

    def GetContainers(self, url, token, endpoints, filterKey="", filterValue=""):
        """
        Gets running or all containers
        url - service url
        token - service API token
        endpoints - Portainer environments
        filterKey - optional "all" - will return stopped containers as well
        filterValue - optional boolean
        """
        headers = {'X-API-Key': f'{token}'}
        containers = []
        filter_keys = {"Names", "Image", "State", "Status"}
        filterValue = "true" if filterKey == "all" else ""
        for endpoint in endpoints:
            url = url + \
                f"api/endpoints/{endpoint}/docker/containers/json?{filterKey}={filterValue}"
            response = requests.get(url, headers=headers).json()
            # Will filter the dictionary to find appropriate values
            def dict_filter(x, y): return dict(
                [(i, x[i]) for i in x if i in set(y)])
            for item in response:
                small_dict = dict_filter(item, filter_keys)
                containers.append([small_dict['Names'][0].replace('/', '').upper(), small_dict['Image'].replace(
                    ":latest", ""), small_dict['State'], small_dict['Status'], str(endpoint)])
        return containers

    def StartContainer(self, url, token, endpoints, name, author_id=None):
        """
        Starts the container
        url - service url
        token - service API token
        endpoints - Portainer environments
        name - container name
        author_id - used to find appropriate values to update in the database 
        """
        headers = {'X-API-Key': f'{token}'}
        containers = self.GetContainers(url, token, endpoints, "all")
        for container in containers:
            if container[0] == name.upper():
                url = url + \
                    f"api/endpoints/{container[4]}/docker/containers/{name.lower()}/restart"
                code = requests.post(url, headers=headers).status_code
                if code == 204:
                    command = f"UPDATE Container_State SET state=1 WHERE user={author_id} AND env='{container[4]}' AND name='{container[0]}'"
                    self.db.DBCommand(command)
                return code
        return 404

    def StopContainer(self, url, token, endpoints, name, author_id=None):
        """
        Stops the container
        url - service url
        token - service API token
        endpoints - Portainer environments
        name - container name
        author_id - used to find appropriate values to update in the database 
        """
        headers = {'X-API-Key': f'{token}'}
        containers = self.GetContainers(url, token, endpoints, "all")
        for container in containers:
            if container[0] == name.upper():
                url = url + \
                    f"api/endpoints/{container[4]}/docker/containers/{name.lower()}/stop"
                code = requests.post(url, headers=headers).status_code
                if code == 204:
                    command = f"UPDATE Container_State SET state=0 WHERE user={author_id} AND env='{container[4]}' AND name='{container[0]}'"
                    self.db.DBCommand(command)
                return code
        return 404

    def ContainerLogs(self, url, token, endpoints, name):
        """
        Gets container logs
        url - service url
        token - service API token
        endpoints - Portainer environments
        name - container name
        """
        headers = {'X-API-Key': f'{token}'}
        containers = self.GetContainers(url, token, endpoints, "all")
        for container in containers:
            if container[0] == name.upper():
                url = url + \
                    f"api/endpoints/{container[4]}/docker/containers/{name.lower()}/logs?stdout=true&stderr=true&tail=25"
                return requests.get(url, headers=headers).content.decode().rstrip('\x02\x00')
        return 404

    def GetStats(self, url, token, endpoints, name):
        """
        Gets container stats
        url - service url
        token - service API token
        endpoints - Portainer environments
        name - container name
        """
        headers = {'X-API-Key': f'{token}'}
        containers = self.GetContainers(url, token, endpoints, "all")
        for container in containers:
            if container[0] == name.upper():
                url = url + \
                    f"api/endpoints/{container[4]}/docker/containers/{name.lower()}/stats?stream=false"
                stats = requests.request("GET", url, headers=headers).json()
                return json.dumps(stats, sort_keys=True, indent=4)
        return 404

    def EmbedMessage(self, ctx, color, title, description):
        responseMessage = discord.Embed(color=color, title=title, description=description).set_author(
            name=ctx.message.author, icon_url=ctx.author.avatar_url)
        return responseMessage


def setup(client):
    client.add_cog(Portainer(client))
    