import logging
from os import getenv

import discord
from cryptocode import encrypt
from discord.ext import commands

import db


class Configure(commands.Cog):
    """
    Main configuration module to register user's services
    """

    def __init__(self, client):
        self.client = client
        self.db = db.Database()

    """ Events """
    # Send message on cog ready
    @commands.Cog.listener()
    async def on_ready(self):
        logging.info(f"configure.py: configure cog is online")

    @commands.Cog.listener()
    async def on_message(self, message):
        """
        Will execute commands upon receiving a message either in a server or in a private message
        """
        if message.author == self.client.user:
            return
        # If it's a direct message
        if not message.guild and message.content.startswith("configure"):
            # Will immediately add user ID to User table
            self.ConfigureDB(message.author.id)
            msg = message.content.split(' ')[1:]
            try:
                # If the parameters are correct, will call ConfigureDB to insert data into Service
                if "https://" in msg[1] and msg[2] is not None:
                    id = message.author.id
                    if msg[0] == "Proxmox":
                        # Encrypts the API key
                        enc = encrypt(msg[2], getenv('ENCRYPT'))
                        self.ConfigureDB(id, msg[0], msg[1], enc)
                        await message.channel.send("Proxmox has been successfully configured :)")
                    elif msg[0] == "Portainer":
                        # Encrypts the API key
                        enc = encrypt(msg[2], getenv('ENCRYPT'))
                        self.ConfigureDB(id, msg[0], msg[1], enc)
                        await message.channel.send("Portainer has been successfully configured :)")
                else:
                    await message.channel.send("That's an invalid configuration")
            except IndexError:
                await message.channel.send("That's an invalid configuration")

    @commands.command()
    async def configure(self, ctx, *args):
        """
        Upon calling ?configure the bot will write a direct message to the user with instructions
        """
        # ctx - Discord context
        message = "To configure a service, send this bot a message with the following format:\n\
        `configure <Service name> <Service URL> <API Key>`\n\
        example:\n\
        ```configure Proxmox https://proxmox.domain.com/ root@pam!Commander=fb1173df-7b25-41d5-9cef-968218916de5```\n\
        ```configure Portainer https://docker.domain.com/ ptr_UTfzylPcWWAxjyrdhkaY1MHj1Z1c+yIDfsadwqq1ZUuc=```"
        emb = discord.Embed(
            title="Configuring a service",
            description=message,
            color=0x696969
        )
        await ctx.send(embed=emb)

    @commands.command()
    async def purge(self, ctx):
        """
        Will remove all user data
        """
        # ctx - Discord context
        command = f"DELETE FROM User WHERE discord_id={ctx.author.id}"
        self.db.DBCommand(command)
        responseMessage = self.EmbedMessage(ctx, color=0x696969, title="Profile", description="The user, their services and VM/Container information removed")
        await ctx.send(embed=responseMessage)

    @commands.command()
    async def profile(self, ctx):
        """
        Will display configured user Services
        """
        # ctx - Discord context
        command = f"SELECT * FROM Service"
        services = self.db.DBCommand(command, retrieveAll=True)
        # If there are no services, say so
        if not services:
            responseMessage = self.EmbedMessage(ctx, color=0x696969, title="Profile", description="No services configured yet")
            await ctx.send(embed=responseMessage)
            return
        message = ""
        # Format and message the user the configured services
        for service in services:
            message += f"**Service name**: {service[1]}\n**URL**: `{service[2]}`\n\n"
        responseMessage = self.EmbedMessage(ctx, color=0x696969, title="Profile", description=message)
        await ctx.send(embed=responseMessage)

    def ConfigureDB(self, id, service=None, URL=None, APIkey=None):
        """
        Will configure the User and Service tables in the database
        id - user discord id
        service - service name (Proxmox or Portainer)
        URL - service url
        APIKey - service API key
        """
        # If there is no user, insert the user first
        command = f"SELECT discord_id FROM User WHERE discord_id = {id}"
        results = self.db.DBCommand(command)
        if results is None:
            command = f"INSERT INTO User (discord_id) VALUES ({id});"
            self.db.DBCommand(command)
            logging.info(f"Writing user {id} into database")

        # If service variable was provided, insert the service
        if service is not None:
            if URL[-1] != "/":
                URL = URL + "/"
            command = f"SELECT url FROM Service WHERE url = '{URL}'"
            results = self.db.DBCommand(command)
            if results is None:
                command = f"INSERT INTO Service (user, name, url, token) VALUES ({id}, '{service}', '{URL}', '{APIkey}');"
                self.db.DBCommand(command)
                logging.info(
                    f"Writing Service {service}, {URL}, {APIkey} for User {id} into database")

    def EmbedMessage(self, ctx, color, title, description):
        responseMessage = discord.Embed(color=color, title=title, description=description).set_author(
            name=ctx.message.author, icon_url=ctx.author.avatar_url)
        return responseMessage


def setup(client):
    client.add_cog(Configure(client))
    
