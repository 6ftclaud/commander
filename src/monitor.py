import json
import logging
from os import environ
from threading import Thread
from time import sleep

import discord
import requests
from cryptocode import decrypt
from discord.ext import commands

import db
import Portainer
import Proxmox


class Monitor(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.db = db.Database()
        self.portainer = Portainer.Portainer(self.client)
        self.proxmox = Proxmox.Proxmox(self.client)
        self.start()

    """ Events """
    # Send message on cog ready
    @commands.Cog.listener()
    async def on_ready(self):
        logging.info("monitor.py: Monitor cog is online")

    def GetServices(self):
        """
        Gets all the configured services from database
        """
        command = f"SELECT * FROM Service"
        return self.db.DBCommand(command, retrieveAll=True)

    def LogServices(self):
        """
        Will compare, insert or update the VM and Container states
        """
        services = self.GetServices()
        for service in services:
            service_name = service[1]
            url = service[2]
            token = decrypt(service[3], environ.get('ENCRYPT'))
            
            if service_name == "Proxmox":
                # Get VMs
                VMs = self.proxmox.GetVM(url, token)
                for VM in VMs:
                    # Get info from database
                    results = self.db.DBCommand(
                        f"SELECT * FROM VM_State WHERE user={service[0]} AND node='{VM['node']}' AND name='{VM['name']}'", retrieveAll=True)
                    state = 1 if VM['status'] == 'running' else 0
                    if results:
                        if results[0][-1] != state:
                            # If the states differ, update the table and notify the user
                            command = f"UPDATE VM_State SET state={state} WHERE user={service[0]} AND node='{VM['node']}' AND name='{VM['name']}'"
                            self.db.DBCommand(command)
                            self.client.loop.create_task(self.NotifyUser(service[0], "Proxmox", VM['name']))
                    # If there are no entries in the database, insert them
                    if not results:
                        command = f"INSERT INTO VM_State (user, node, name, state) VALUES ({service[0]}, '{VM['node']}', '{VM['name']}', {state})"
                        self.db.DBCommand(command)

            if service_name == "Portainer":
                # Get containers
                environments = self.portainer.GetEndpoints(
                    url, token)
                containers = self.portainer.GetContainers(
                    url, token, environments, "all")
                for container in containers:
                    container[0] = container[0].lower()
                    user = service[0]
                    env = container[4]
                    name = container[0]
                    # Get info from database
                    results = self.db.DBCommand(
                        f"SELECT * FROM Container_State WHERE user={user} AND env='{env}' AND name='{name}'", retrieveAll=True)
                    state = 0 if container[2] == 'exited' else 1
                    if results:
                        if results[0][-1] != state:
                            # If the states differ, update the table and notify the user
                            command = f"UPDATE Container_State SET state={state} WHERE user={user} AND env='{env}' AND name='{name}'"
                            self.db.DBCommand(command)
                            self.client.loop.create_task(self.NotifyUser(service[0], "Portainer", name))
                    # If there are no entries in the database, insert them
                    if not results:
                        command = f"INSERT INTO Container_State (user, env, name, state) VALUES ({user}, '{env}', '{name}', {state})"
                        self.db.DBCommand(command)


    async def NotifyUser(self, id, platform, name):
        """
        Notifies the user of any changes
        id - user id
        platform - Proxmox or Portainer
        name - VM/Container name
        """
        user = await self.client.fetch_user(id)
        emb = discord.Embed(
            title="State changed",
            description=f"**{platform}** service **{name}** has changed state",
            color=discord.Color.orange()
        )
        await user.send(embed=emb)


    def start(self):
        """
        Starts the thread to monitor services every minute
        """
        t = Thread(target=self.run)
        t.start()

    def run(self):
        while True:
            logging.info("Commander daemon checking VM/Container state")
            self.LogServices()
            sleep(60)

def setup(client):
    client.add_cog(Monitor(client))
    