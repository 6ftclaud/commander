# Use the Python 3.10.5 image
FROM python:3.10.5

# Work in /srv/commander
WORKDIR /srv/commander/

# Copy the python source files and requirements
COPY src/*.py ./
COPY requirements.txt .

# Update the image
RUN apt update \
    && apt upgrade -y

# Don't use root user
RUN groupadd -r abc && useradd -m --no-log-init -r -g abc abc
RUN chown -R abc:abc /srv/commander/
USER abc

# Install dependencies
RUN pip install -r requirements.txt \
    && rm requirements.txt

# Run the bot
ENTRYPOINT ["python", "main.py"]
